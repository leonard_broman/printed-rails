
use <fixator.scad>
use <track.scad>

render() 
//ClassicEndStop();
ClassicMaleEndstop();

module ClassicEndStop() {
    difference() {
        translate([0,-20,0])
        rotate([0,-90,-90])
        linear_extrude(height=40) polygon(points=[
            [0,0],
            [0,45],
            [14,45],
            [28,45-(28-14)],
            [28,8],
            [18,8],
            [18,20],
            [12,20],
            [12,0]
        ]);
        FemaleFixationHole(extra_neck_length=1);
    }
}

module ClassicMaleEndstop() {
    difference() {
        translate([0,-20,0])
            rotate([0,-90,-90])
            linear_extrude(height=40) polygon(points=[
                [0,0],
                [0,45],
                [14,45],
                [28,45-(28-14)],
                [28,8],
                [18,8],
                [18,20],
                [12,20],
                [12,0]
            ]);
        translate([8,0,0])
        rotate([0,-90,0])
        linear_extrude(height=11) FullTrackProfile(track_cutouts=true);
        
    }
    
    Fixator(height=8);
}
