
x_offset = 16;
y_offset= 44;

linear_extrude(height=2) translate([0,0,0]) TrackProfile(extra_depth=0);
linear_extrude(height=2) translate([0,-20,0]) TrackProfile(symetrical=true,extra_depth=0);
linear_extrude(height=2) translate([0,20,0]) IronTrackProfile(symetrical=false);
linear_extrude(height=2) translate([-x_offset*1,-y_offset*1,0]) FullTrackProfile(profile=true);
linear_extrude(height=2) translate([-x_offset*1,0,0]) FullTrackProfile();
linear_extrude(height=2) translate([-x_offset*1,y_offset*1,0]) FullTrackProfile(track_cutouts=true);
linear_extrude(height=2) translate([-x_offset*1,y_offset*2,0]) FullTrackProfile(track_cutouts=true,passage_cutouts=true);

linear_extrude(height=2) translate([-x_offset*2,-y_offset*1,0]) FullTrackProfile(symetrical=true,profile=true);
linear_extrude(height=2) translate([-x_offset*2,0,0]) FullTrackProfile(symetrical=true);
linear_extrude(height=2) translate([-x_offset*2,y_offset*1,0]) FullTrackProfile(symetrical=true,track_cutouts=true);
linear_extrude(height=2) translate([-x_offset*2,y_offset*2,0]) FullTrackProfile(symetrical=true,track_cutouts=true,passage_cutouts=true);

linear_extrude(height=2) translate([-x_offset*3,-y_offset*1,0]) FullTrackProfile(iron_track=true,profile=true);
linear_extrude(height=2) translate([-x_offset*3,0,0]) FullTrackProfile(iron_track=true);
linear_extrude(height=2) translate([-x_offset*3,y_offset*1,0]) FullTrackProfile(iron_track=true,track_cutouts=true);
linear_extrude(height=2) translate([-x_offset*3,y_offset*2,0]) FullTrackProfile(iron_track=true,passage_cutouts=true);


//translate([0,0,0]) mirror([0,0,0]) TrackSlot();
//translate([0,-35,0]) BottomTrackSlot();

module PlaceTracks() {
    second_track_offset=26;
    translate([0,second_track_offset,0]) children();
    translate([0,0,0]) children();
}

module FullTrackProfile(symetrical=false,iron_track=false,track_cutouts=false,passage_cutouts=false,profile=undef) {
    total_width=40;
    second_track_offset=26;
    translate([0,-total_width/2,0])
    if (track_cutouts || passage_cutouts) {
            PlaceTracks() TrackCutouts(symetrical=symetrical,track=track_cutouts,passages=passage_cutouts,iron=iron_track);
    } else {
        if (!iron_track) {
            PlaceTracks() TrackProfile(symetrical=symetrical,profile=profile);
        } else {
            PlaceTracks() IronTrackProfile(profile=profile);
        }
    }
}

module TrackSlot() {
    height = 0;
    track_depth = 3;
    polygon(points=[
            [height+0.5,3.5]
            ,[height-track_depth,4] // Top track
            ,[height-track_depth,10]
            ,[height+0.5,10.5]
    ]);
}

module BottomTrackSlot() {
    extra_depth=0.5;
    hull() {
        polygon(points=[
            [-extra_depth,11]
                ,[5,9]
                ,[5,5]
                ,[-extra_depth,3]
        ]);
        translate([5,7,0]) circle(2,$fn=30);
    }
}

module OuterTrackProfile(extra_depth=0) {
    height = 12;
    track_depth = 3;
    polygon(points=[
            [-extra_depth,0]
            //,[12,0]
            ,[height-1,0]
            ,[height,1] // Top corner
            ,[height,13]
            ,[height-1,14]
            ,[1-extra_depth,14]
            ,[0-extra_depth,13]
            ,[0-extra_depth,1] 
            ,[1-extra_depth,0]
    ]);
}

module TrackCutouts(height=12,symetrical=false,track=true,passages=true,iron=undef) {
    extra_depth = height - 12;
    if (iron == true) {
        translate([height,0,0]) TrackSlot();
    } else 
    if (symetrical) {
        if(track) {
            translate([height,0,0]) TrackSlot();
            translate([-extra_depth,0,0]) mirror([1,0,0]) TrackSlot();
        }
        if (passages) {
            hull() {
                translate([height/2,7,0]) circle(2,$fn=30);
                if (extra_depth > 0) {
                    translate([height/2-extra_depth,7,0]) circle(2,$fn=30);
                }
            }
        }
    } else {
        if (track) {
            translate([height,0,0]) TrackSlot();
        }
        if (passages) {
            translate([0,0,0]) BottomTrackSlot(extra_depth=extra_depth);
        }
    }
}

module TrackProfile(extra_depth=0,symetrical=false,profile=undef) {
    height = 12;
    if (profile==true) {
        OuterTrackProfile(extra_depth=extra_depth);
    } else {
        difference() {
                OuterTrackProfile(extra_depth=extra_depth);
                TrackCutouts(height=height,symetrical=symetrical);
        }
    }
}

module IronTrackCutout(height=12) {
    center_width = 2;
    cutout_depth = (14 - center_width)/2;
    top_track_offset = 3;
    track_depth = 3;
    polygon(points=[
        [1.5,0]
        ,[3,cutout_depth]
        ,[height-track_depth-top_track_offset,cutout_depth]
        ,[height-track_depth-top_track_offset+cutout_depth-1,1]
        ,[height,1]
        ,[height,0]
    ]);
}

module IronTrackProfile(height=12,symetrical=false,profile=undef) {
    if (profile == true) {
        difference() {
            OuterTrackProfile(extra_depth=height-12);
            IronTrackCutout(height=height);
            translate([0,14,0]) mirror([0,1,0]) IronTrackCutout(height=height); 
        }
    } else if (symetrical) {
        echo("Symetrical iron tracks not supported!");
    } else {
        difference() {
            OuterTrackProfile(extra_depth=height-12);
            IronTrackCutout(height=height);
            translate([0,14,0]) mirror([0,1,0]) IronTrackCutout(height=height); 
            translate([height,0,0]) TrackSlot();
        }
        
        
    }
    
}


