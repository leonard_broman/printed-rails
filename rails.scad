
use <fixator.scad>
use <track.scad>
use <parts.scad>
use <utils.scad>


/*
    FIXME Base bent generator on center radius, and center the track. This helps alot with left turns as they are otherwise generated from a shorter inner radius.
    
    
PAppA
Arthur
MAMMA
Gustave
*/

/* 
    Standard bent piece: l=160;r=45
    Inner bent peice: l=128,r=45
    Standard short bent piece: l=84 r=45 (this is same center radius as brio wooden dual car/vehicle piece)
    
    Standard straight piece: l=215
    Standard shorter peice: 143
    Standard even shorter piece: 117
    
    Approximate lengths that match lego piece lengths: 215 -> 208 ,143 -> 136, 117 -> 112
    
    Useful down to about 35 cm for bent pieces.
*/
//parameter_bend = true;
//parameter_bend_right = false;
//CUSTOMIZER VARIABLES
// Angles below 10 deg may take a long time to render.
parameter_bend_angle = 45; 
// Makes track usable on both sides, not fully supported
parameter_symetrical = false;
// Track length
parameter_length = 160; // [40:300]
parameter_iron_track = false; // Not fully supported for bent tracks
parameter_crossing = false; //Only works for straight tracks (and not even iron tracks)
parameter_female_end = true;
parameter_lego_stud_lines = 2;

parameter_3_junction = false; // This still produces incorrect models

//CUSTOMIZER VARIABLES END

echo("Angle, symetrical, ...",parameter_bend_angle,parameter_symetrical,parameter_length,parameter_crossing,parameter_female_end,parameter_3_junction);

length = parameter_length; // Deprecated, can cause interference with local variables
track_length = parameter_length;
3_junction = parameter_3_junction;
3_junction_angle = 60;
3_junction_length = 80;

// add "extra_depth" to thicken the bottom of the piece, gives more space for cabling and somewhat more stable platform
// This has turned out quite useless and is no longer maintained
extra_depth=0;
female_end=parameter_female_end;
bend = parameter_bend_angle != 0 ? true : false ;
bend_angle= parameter_bend_angle;
symetrical = parameter_symetrical;
crossing = bend ? false : parameter_crossing;
iron_track = parameter_iron_track;
// Decrease sleeper distance with increasing bend angle 1/45 looks good enough
sleeper_distance = 10-(bend ? bend_angle/45 : 0);
lego_stud_lines = parameter_lego_stud_lines;
// "Constants", don't fiddle here!
single_track_width=14;
rail_bottom_height = 9;
fixation_offset = 10.5;
total_width=40;
total_height=12;
track_gap = 12;
second_track_offset = single_track_width + track_gap;
track_distance = 26; // This should be 12, but the variable is still used incorrectly

echo("Angle, symetrical, ...",bend_angle,symetrical,track_length,crossing,female_end,3_junction);

sleepers = (length-fixation_offset*2-4)/sleeper_distance;

pi = pi();
//bend_radius=(length*360)/(bend_angle*2*pi());


function radius(length,angle) = (length*360)/(angle*2*pi());
function arc_angle(length,radius) = (length*360)/(2*pi()*radius);
function arc_length(radius,angle) = (2*pi()*radius*angle)/360;
function is_bend(angle) = angle > 10 || angle < -10; // Bend threshold

bend_radius = radius(length,bend_angle);
center_bend_radius = bend_radius;
inner_bend_radius = bend_radius - total_width/2;
outer_bend_radius = inner_bend_radius + total_width;
echo("Radius (center, inner, outer):",center_bend_radius,inner_bend_radius,outer_bend_radius);

x_outer_length = outer_bend_radius * sin(bend_angle);
x_inner_length = inner_bend_radius * sin(bend_angle);
echo("Outer x length: ", x_outer_length);

//echo("Radius ", radius(156-42,45));
echo("Arc length ", arc_length(radius(156-42,45),45));

// TODO This can be calculated better
track_sweep_fn=(360/abs(bend_angle))*10;

if (bend) {
    echo("Producing bent rails.");
    echo("Bend radius: ", bend_radius);
    echo("Inner bend radius: ", inner_bend_radius);
    echo("Center bend radius: ", center_bend_radius);
    echo("Outer bend radius: ", outer_bend_radius);
    echo("Track sweep steps:", track_sweep_fn);
} else {
    echo("Producing straight track.");
    echo("Track length: ",length);
    if (crossing) {
        echo("Adding crossing.");
    }
}

/*

MaleFixation();

*/

//render() SimpleStraightRails(length=100);

if (bend) {
    BentRails();
} else {
     StraightRails();
} 

module SimpleStraightRails(length=undef,symetrical=false) {
    _length = length == undef ? 60 : length;
    difference() {
        union() {
            StraightTracks(profile=true,iron=iron_track,length=_length,symetrical=symetrical);
            StraightSleepers(length=_length,symetrical=symetrical);
            EndBlock(end=true,length=_length);
            EndBlock(end=false,length=_length);
        }
        
        translate([0,0,0]) FemaleFixationHole(extra_neck_length=1);
        translate([_length,0,0]) mirror([1,0,0]) FemaleFixationHole(extra_neck_length=1);
        
        translate([-1,0,0]) StraightTracks(cutouts_only=true,iron=iron_track,symetrical=symetrical,length=_length+2);
    }
    if (!symetrical) {
        TrackTicks(length=_length) BottomTrackSupport(sym=symetrical);
    }
}

module StraightRails() {
    difference() {
        union() {
            StraightTracks(profile=true,iron=iron_track);
            StraightSleepers();
            if (female_end) {
                EndBlock();
            } else {
                EndBlock(endblock_depth=10);
                translate([length,0,0]) Fixator(half=true,pinhole=false);
            }
            EndBlock(end=false);
            
            
            if (crossing) {
                CrossingRails();
            }
            
            if (3_junction) {
                BentTracks2(3_junction_length,3_junction_angle,profile=true,iron=iron_track);
                BentTracks2(3_junction_length,-3_junction_angle,profile=true,iron=iron_track);
                EndBlock(end=true,angle=3_junction_angle,length=3_junction_length);
                EndBlock(end=true,angle=-3_junction_angle,length=3_junction_length);
            }
            
        }
        
        translate([0,0,0]) FemaleFixationHole(extra_neck_length=1);
        if (female_end) {
            translate([length,0,0]) mirror([1,0,0]) FemaleFixationHole(extra_neck_length=1);
            
        } else {

        }
        
        if (3_junction) {
           FemaleFixationHole2(end=true,length=3_junction_length,angle=3_junction_angle);
           FemaleFixationHole2(end=true,length=3_junction_length,angle=-3_junction_angle);
        }
        
        if (3_junction) {
            // Cut out the filled tracks
            BentTracks2(3_junction_length,3_junction_angle,cutouts_only=true,iron=iron_track,symetrical=symetrical);
            BentTracks2(3_junction_length,-3_junction_angle,cutouts_only=true,iron=iron_track,symetrical=symetrical);
        }
        StraightTracks(cutouts_only=true,iron=iron_track,symetrical=symetrical);
        
        if (crossing) {
            translate([length/2,total_width/2+1,0]) rotate([0,-90,90])
            linear_extrude(height=total_width+2) FullTrackProfile(track_cutouts=true,symetrical=symetrical);

            translate([length/2,-total_width/2-16,0])
            rotate([0,0,90]) FemaleFixationHole(extra_neck_length=1);
            
            translate([length/2,total_width/2+16,0])
            rotate([0,0,-90]) FemaleFixationHole(extra_neck_length=1);
  
        } 
    }
    
    if (crossing && !symetrical) {
        // Add bottom bars to avoid a easily crackable top rail surface.
        
        for(i=[-1,0,1]) {
                translate([length/2-(total_width-2)/2,i*total_width/2-2,0]) cube([single_track_width-2,4,2]);
            }
        for(i=[-1,0,1]) {
                translate([length/2+track_gap/2+1,i*total_width/2-2,0]) cube([single_track_width-2,4,2]);
        }
        
    }
    
    // Add track spacing support
    TrackTicks(length=track_length) BottomTrackSupport();
    if (3_junction) {
        TrackTicks(length=3_junction_length,angle=3_junction_angle) BottomTrackSupport();
        TrackTicks(length=3_junction_length,angle=-3_junction_angle) BottomTrackSupport();
    }
    
    if (lego_stud_lines > 0) {
        FLU = 1.6; // Fundamental Lego Unit = 1.6 mm
        BRICK_WIDTH = 5*FLU;
        stud_count = floor(length / (5*FLU));
        inner_fitting_width = (floor(total_width/BRICK_WIDTH) + 1)*BRICK_WIDTH;
        
        stud_count_remainder = length - 5*FLU*stud_count;
        echo("Lego stud length: ", 5*FLU*stud_count);
        echo("Adding lego studs (lines,count) ",lego_stud_lines,stud_count);
        translate([stud_count_remainder/2,inner_fitting_width/2 + (0)*5*FLU,0]) BrickTop(width=lego_stud_lines,length=stud_count,bottom_chamfer=true);
        translate([stud_count_remainder/2,total_width/2,0]) cube([length,inner_fitting_width/2-total_width/2,6*FLU]);
        translate([stud_count_remainder/2,-inner_fitting_width/2,0]) cube([length,inner_fitting_width/2-total_width/2,6*FLU]);
        translate([stud_count_remainder/2,-inner_fitting_width/2 - (lego_stud_lines)*5*FLU,0]) BrickTop(width=lego_stud_lines,length=stud_count,bottom_chamfer=true);
    }
    
}

module BentRails() {
    fixator_height = symetrical ? total_height : 8;
    y = cos(bend_angle)*bend_radius;
    x = sin(bend_angle)*bend_radius;
    
    difference() {
            // End fixation position
            x = sin(bend_angle)*center_bend_radius;
            y = cos(bend_angle)*center_bend_radius - center_bend_radius;
            union() {
                BentTracks();
                BentSleepers();
                if (female_end) {
                    EndBlock(end=true);
                } else {
                    EndBlock(end=true,endblock_depth=10);
                    //translate([x,y,total_height/2-fixator_height/2]) // fixator_height = 8
                    //rotate([0,0,-bend_angle])
                    PlaceBentItem(bend_angle,radius=center_bend_radius)
                    Fixator(half=true,height=fixator_height,pinhole=false);
                }
                EndBlock(end=false);
            }
            
            translate([0,0,0]) FemaleFixationHole(extra_neck_length=2);
            
            if (female_end) {
                translate([x,y,0]) 
                rotate([0,0,-bend_angle])
                mirror([1,0,0]) 
                FemaleFixationHole(extra_neck_length=2);
            } else {
            }
        }
    // Add track spacing support
    TrackTicks(length=track_length,angle=bend_angle) BottomTrackSupport();
        
    if (lego_stud_lines > 0) {
        echo("Adding lego stud lines to bent rails, highly experimental");
        FLU = 1.6; // Fundamental Lego Unit = 1.6 mm
        BRICK_WIDTH = 5*FLU;
        outer_stud_count = floor(x_outer_length / (5*FLU));
        inner_stud_count = floor(x_inner_length / (5*FLU));
        inner_fitting_width = (floor(total_width/BRICK_WIDTH) + 1)*BRICK_WIDTH;
        outer_stud_count_remainder = x_outer_length - 5*FLU*outer_stud_count;
        inner_stud_count_remainder = x_inner_length - 5*FLU*inner_stud_count;
        // Studs on the outer side of the bend
        
        for ( x = [outer_stud_count_remainder : 5*FLU : x_outer_length - 5*FLU ] ) {
            arc_offset = outer_bend_radius - sqrt(pow(outer_bend_radius,2)-pow(x,2));
            echo("Adding stud at (x,offset)", x,arc_offset);
            stud_offset = floor(arc_offset/ (5*FLU))*5*FLU;
            translate([x,total_width/2-stud_offset,0]) BrickTop(width=lego_stud_lines,length=1,bottom_chamfer=false);
        }
        //BendExtrude(arc_length(outer_bend_radius,bend_angle),0,bend_angle,height=5,fn=10,radius=outer_bend_radius) square(size=[5*FLU,6*FLU]);
        
        
        translate([0,-bend_radius,0]) linear_extrude(height=6*FLU) {
            difference() {
                PieceOfPie(outer_bend_radius+8*FLU,0,bend_angle);
                PieceOfPie(outer_bend_radius,0,bend_angle);
            }
        }
        
        // Studs on the inner side of the bend
        // Use of outer remained is on purpose to align studs on the inner and outer side
        for ( x = [outer_stud_count_remainder : 5*FLU : x_inner_length - 2*5*FLU ] ) {
            arc_offset = inner_bend_radius - sqrt(pow(inner_bend_radius,2)-pow(x+5*FLU,2));
            echo("Adding stud at (x,offset)", x,arc_offset);
            stud_offset = floor((arc_offset + 5*FLU) / (5*FLU))*5*FLU;
            translate([x,-total_width/2-stud_offset-5*FLU*lego_stud_lines,0]) BrickTop(width=lego_stud_lines,length=1,bottom_chamfer=false);
        }
        
        translate([0,-bend_radius,0]) linear_extrude(height=6*FLU) {
            difference() {
                PieceOfPie(inner_bend_radius,0,bend_angle);
                PieceOfPie(inner_bend_radius-8*FLU,0,bend_angle);
            }
        }
        
    }
}

module FemaleFixationHole2(end = true,length=undef,angle=undef) {
    _length = length == undef ? track_length : length;
    _angle = angle == undef ? bend_angle : angle;
    _radius = angle == undef ? center_bend_radius : radius(_length,_angle);
    x = sin(_angle)*_radius;
    y = cos(_angle)*_radius - _radius;
    translate([x,y,0]) 
    rotate([0,0,-_angle])
    mirror([1,0,0]) 
    FemaleFixationHole(extra_neck_length=2);

}


module CrossingRails() {
    extension_length = 18;
    if (!iron_track) {
        translate([length/2,-total_width/2+2,0]) rotate([0,-90,90])
        linear_extrude(height=extension_length) FullTrackProfile(extra_depth=extra_depth,symetrical=symetrical);
    
        translate([length/2,track_gap/2+2,0]) rotate([0,-90,90])
        linear_extrude(height=track_gap+4) FullTrackProfile(extra_depth=extra_depth,symetrical=symetrical);
        
        translate([length/2,total_width/2-2+extension_length,0]) rotate([0,-90,90])
        linear_extrude(height=extension_length) FullTrackProfile(extra_depth=extra_depth,symetrical=symetrical);
        
        translate([length/2,-extension_length-total_width/2+2,0])
        rotate([0,0,90])
        translate([0,0,0])
        EndBlock(end=false,endblock_depth=extension_length);
        
        translate([length/2,total_width/2-2,0])
        rotate([0,0,90])
        translate([0,0,0])
        EndBlock(end=false,endblock_depth=extension_length);
        
        
    }
}

module StraightTracks(length=undef,cutouts_only=undef,profile=undef,iron=undef,symetrical=false) {
    _length = length == undef ? track_length : length;
    translate([_length,0,0])
            rotate([0,-90,0])
                linear_extrude(height=_length) {
                    FullTrackProfile(track_cutouts=cutouts_only,passage_cutouts=cutouts_only,profile=profile,iron_track=iron,symetrical=symetrical);
                }
}

module StraightSleepers(length=undef,symetrical=undef) {
    TrackTicks(length == undef ? track_length : length) Sleeper(symetrical=symetrical);
}

module TrackTicks(length,angle=0) {
    // Decrease sleeper distance with increasing bend angle 1/45 looks good enough
    tick_distance = 10-(is_bend(angle) ? abs(angle/45) : 0);
    echo("Tick distance ",tick_distance);
    ticks = (length-fixation_offset*2-4)/tick_distance;
    if (is_bend(angle)) {
        echo("Ticking bend...");
        radius = radius(length,angle);
        echo("Bend radius ",radius);
        center_angle = angle/2;
        tick_angle = arc_angle(tick_distance,radius);
        echo("Tick angle ",tick_angle);
        PlaceBentItem(center_angle,radius) children();
        for(i = [1:1:ticks/2]) {
            PlaceBentItem(center_angle-tick_angle*i,radius) children();
            PlaceBentItem(center_angle+tick_angle*i,radius) children();
        } 
    } else {
        echo("Ticking straight...");
        y_offset=0;
        center = length/2; // Sleeper created on positive x-axis, not centered
        translate([center,y_offset,0]) children();
        for(i = [0:1:ticks/2]) {
            translate([center-tick_distance*i,y_offset,0]) children();
            translate([center+tick_distance*i,y_offset,0]) children();
        } 
    }
}

module BentTracks2(length,angle,cutouts_only=false,profile=undef,iron=undef,symetrical=false) {
    BendExtrude(length,0,angle,fn=10) rotate([0,0,90]) FullTrackProfile(track_cutouts=cutouts_only,passage_cutouts=cutouts_only,profile=profile,iron_track=iron,symetrical=symetrical);
}

module BentTracks() {
    direction = bend_angle>0 ? 1 : -1;
    // Single track with because of the badly positioned TrackProfile after rotation
    inner_bend_offset = inner_bend_radius + single_track_width; 
    // Not same as outer_bend_radius, this is the offset of the inner edge of the outer track
    outer_bend_offset = inner_bend_offset + track_distance; 
 /*
    linear_extrude(height=10) translate([single_track_width,0,0]) rotate([0,0,90]) TrackProfile(extra_depth=extra_depth,symetrical=symetrical);
   */ 
    translate([0,-bend_radius,0])
    difference() {
        $fn=track_sweep_fn;
        rotate_extrude() {
            if (!iron_track) {
                translate([inner_bend_offset,0,0]) rotate([0,0,90]) TrackProfile(extra_depth=extra_depth,symetrical=symetrical);
                translate([outer_bend_offset,0,0]) rotate([0,0,90]) TrackProfile(extra_depth=extra_depth,symetrical=symetrical);
            } else {
                translate([inner_bend_offset,0,0]) rotate([0,0,90]) IronTrackProfile(extra_depth=extra_depth,symetrical=symetrical);
                translate([outer_bend_offset,0,0]) rotate([0,0,90]) IronTrackProfile(extra_depth=extra_depth,symetrical=symetrical);
            }
        }
        
        SweepCutter();
    } 
    
}


module SweepCutter() {
    start = bend_angle > 0 ? 0 : 180;
    end = bend_angle > 0 ? bend_angle : 180 + bend_angle;
    translate([0,0,-1])
    linear_extrude(height=total_height+2)
    SweepCutPolygon(outer_bend_radius,start_angle=start,end_angle=end);
}

module BentSleepers() {
    //center = length/2-2; // Sleeper created on positive x-axis, not centered
    center_angle = bend_angle/2;
    sleeper_angle = arc_angle(sleeper_distance,center_bend_radius);
    
    PlaceBentSleeper(center_angle);
    for(i = [1:1:sleepers/2]) {
        PlaceBentSleeper(center_angle-sleeper_angle*i);
        PlaceBentSleeper(center_angle+sleeper_angle*i);
    } 
}

module PlaceBentItem(angle,radius) {
    y = cos(angle)*radius - radius;
    x = sin(angle)*radius;
    translate([x,y,0]) // Move into position on the track
    rotate([0,0,-angle]) // Rotate around center
    //translate([0,-total_width/2,0]) // Center sleeper before rotating
    children();    
}

module PlaceBentSleeper(angle) {
    echo("Sleeper angle " , angle);
    PlaceBentItem(angle,center_bend_radius) Sleeper();
}

/*
    Length is the total track length
*/
module EndBlock(end = true,endblock_depth=20,angle=0,length=undef) {
    if (angle!=0) {
        radius = radius(length,angle);
        SweptEndBlock(end,endblock_depth,angle=angle,_radius=radius);
    } else if (bend) {
        SweptEndBlock(end,endblock_depth);
    } else {
        StraightEndBlock(end,endblock_depth,length=length);
    }
}

module StraightEndBlock(end = true,endblock_depth=20,length=undef) {
    //height = 
    _track_length = length == undef ? track_length : length;
    width = iron_track ? 18 : 14;
    height = symetrical ? total_height : 10;
    dimensions = iron_track ? [endblock_depth,width,height+extra_depth] : [endblock_depth,width,height+extra_depth];
    height_offset = symetrical ? 0 : 0;
    mirror_vector = end ? [0,0,0] : [1,0,0];
    positioning_vector = end ? [_track_length, 0,height_offset] : [0,0,height_offset];
    translate(positioning_vector) 
    mirror(mirror_vector)
    translate([-endblock_depth,-width/2,-extra_depth]) cube(dimensions);
}

module SweptEndBlock(end = true, endblock_depth=20,angle=undef,_radius=undef) {
    block_width = 16;
    block_height = symetrical ? total_height : 10;
    if (_radius != undef) {
        echo("Sweeping endblock with new style parameters");
        sweep_angle = arc_angle(endblock_depth,_radius);
        start_angle=end ? angle - sweep_angle : 0;
        end_angle=end ? angle : angle - sweep_angle;
        BendExtrude(endblock_depth,start_angle,end_angle,radius=_radius) 
            translate([-block_width/2,0,0])
            square([block_width,block_height]);
    } else {
        radius = center_bend_radius;
        sweep_angle = arc_angle(endblock_depth,radius);
        start_angle=bend_angle > 0 ? (end ? bend_angle - sweep_angle : 0) : (end ? 180 + (bend_angle - sweep_angle) : 180) ;
        end_angle=bend_angle > 0 ? (end ? bend_angle : sweep_angle) : (end ? 180 + bend_angle : 180 + sweep_angle);
        echo("Endblock sweep angle ", end, sweep_angle, start_angle, end_angle);
        translate([0,-radius,0])
        difference() {
            $fn=100;
            rotate_extrude() {
                translate([radius-block_width/2,0,0])
                square([block_width,block_height]);
            }
            translate([0,0,-1]) linear_extrude(height=block_height+2) SweepCutPolygon(radius=radius,end_angle=end_angle,start_angle=start_angle,fn=6);
        }
        
    }    
}

module FemaleFixation(end = true) {
    if (bend) {
 
    } else {
        endblock_depth=15;
        mirror_vector = end ? [0,0,0] : [1,0,0];
        positioning_vector = end ? [length,0,0] : [0,0,0];
        translate(positioning_vector) 
        mirror(mirror_vector)
        union() {
            difference() {
                translate([-endblock_depth,14,-2]) cube([endblock_depth,12,10]);
                FemaleFixationHole();
            }
        }
    }
}

module MaleFixation() {

    difference() {
        union() {
            translate([-fixation_offset,20-3.5,0]) cube([fixation_offset*1.5,7,rail_bottom_height]);
            translate([-4,0,-2]) cube([4,40,2]);
            translate([-fixation_offset,20,0]) cylinder(r=6.5,h=10);
        }
        translate([-fixation_offset,20,-5]) cylinder(r=4,h=16);
    };

}

/*
* Use "sym" here to avoid interference with the global variable. The global should probable be renamed.
*/
module BottomTrackSupport(sym=undef) {
    _symetrical = sym == undef ? symetrical : sym;
    if (!_symetrical && !iron_track) {
        translate([0-2,2-total_width/2,0])
        cube([4,single_track_width-4,1]);
        translate([0-2,2+track_gap/2,0])
        cube([4,single_track_width-4,1]);
    }
}

module Sleeper(sym=undef) {
    _symetrical = sym == undef ? symetrical : sym;
    sleeper_length = iron_track ? 24 : 14;
    asymetrical_profile = [[-extra_depth,0],[5,1],[5,3],[-extra_depth,4]];
    if (_symetrical) {
        translate([0+2,sleeper_length/2,-extra_depth])
        rotate([90,-90,0])
        linear_extrude(height=sleeper_length)
        polygon(points=[
            [0,1],[total_height/2,0],[total_height,1],[total_height,3],[total_height/2,4],[0,3]
        ]);
    } else if (!iron_track) {
        translate([4-2,sleeper_length/2,0])
        rotate([90,-90,0])
        linear_extrude(height=sleeper_length)
        polygon(points=asymetrical_profile);
        
    } else {
        translate([0-2,1-total_width/2,-extra_depth])
        cube([4,total_width-2,1]);
        translate([4-2,sleeper_length/2,0])
        rotate([90,-90,0])
        linear_extrude(height=sleeper_length)
        polygon(points=asymetrical_profile);
    }
    
}





