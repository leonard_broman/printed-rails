
radius = 5;
//SweepCutPolygon(radius,90,0);
/*translate([radius*2*2.5,0,0]) SweepCutPolygon(radius,-90,0);
translate([radius*2*2.5-20,0,0]) rotate([0,0,90]) SweepCutPolygon(-radius,180,166.003);
translate([0,radius*2*2.5,0]) SweepCutPolygon(radius,45,-45); */
//translate([0,radius*2*2.5*2,0]) SweepCutPolygon(radius,255,0);

//translate([0,radius*2*2.5*3,0]) PieceOfPie(radius*2,start=0,end=90);
//translate([radius*2*2.5,radius*2*2.5*3,0]) PieceOfPie(radius*2,start=75,end=-85);
/*
linear_extrude(height=1) translate([0,0,0]) RotateCutPolygon(radius,0,45,fn=20);
linear_extrude(height=1) translate([radius*2.5,0,0]) RotateCutPolygon(radius,0,-45,fn=20);
linear_extrude(height=1) translate([0,radius*2.5,0]) RotateCutPolygon(radius,-45,45,fn=20);
linear_extrude(height=1) translate([radius*2.5,radius*2.5,0]) RotateCutPolygon(radius,45,-45,fn=20);
linear_extrude(height=1) translate([0,radius*5,0]) RotateCutPolygon(radius,-1,0,fn=20);
linear_extrude(height=1) translate([radius*2.5,radius*5,0]) RotateCutPolygon(radius,0,359,fn=20);
*/
render() BendExtrude(25,-25,-45,height=5,extra_radius=5,fn=10,radius=-25) translate([-2.5,0,0]) square(size=[3,5]);
    
function cat(L1, L2) = [for(L=[L1, L2], a=L) a];
function reverse(L1) = [for (i = [len(L1)-1:-1:0]) L1[i]];
function pi() = 3.14;

/*
    Crates a cutting profile to cut a sweep into a segment
    Works for angles -90 to 90 degrees.

*/
module SweepCutPolygon(radius,end_angle,start_angle=0,fn=20) {
     mult=3;
     outer_edge = radius*mult*2;
     difference() {
         circle(abs(radius)*mult*0.75);
         PieceOfPie(abs(radius)*mult,start=start_angle,end=end_angle,fn=fn);
     }
}

module RotateCutPolygon(radius,start,end,fn=20) {
    s1 = start > end ? end : start;
    e1 = start > end ? start : end;
    the_start = e1;
    the_end = s1 + 360;
    end_offs = start > end ? 0.1 : -0.1;
    angles = cat([for(i=[the_start:abs(the_end-the_start) / fn:the_end-0.1]) i ],[the_end]);
    //echo("Angles: ", angles);
    points = cat([[0,0]] , [ for(i=angles) let(x=radius*sin(i),y=radius*cos(i)) [x,y]]);
    //echo("Arc points ", points);
    polygon(points=points);
}

module BendExtrude(length,start,end,height=20,extra_radius=20,fn=10,radius=undef) {
    the_fn = fn * (360/abs(start-end));
    angle=end-start;
    radius1=radius == undef ? (length*360)/(angle*2*pi()) : radius;
    outer_radius=radius1 > 0 ? radius1*1.5 + extra_radius + 1 : radius1*1.5 - extra_radius - 1;
    translate([0,-radius1,0])
    difference() {
        $fn=the_fn;
        rotate_extrude() {
            translate([radius1,0,0]) children();
        }
        translate([0,0,-1]) linear_extrude([height+2,0,0]) RotateCutPolygon(outer_radius,start,end);
    }
    //translate([0,0,-1]) linear_extrude([height+2,0,0]) RotateCutPolygon(outer_radius,start,end);
}


module PieceOfPie(radius,start=0,end=90,fn=20) {
    the_start = start > end ? end : start;
    the_end = start > end ? start : end;
    echo(arc_start=the_start,arc_end=the_end);
    // The concatenated last value is because of a rounding bug that causes the last value in the list to sometimes get excluded
    angles = cat([for(i=[the_start:abs(the_end-the_start) / fn:the_end-0.1]) i ],[the_end]);
    echo("Angles: ", angles);
    points = cat([[0,0]] , [ for(i=angles) let(x=radius*sin(i),y=radius*cos(i)) [x,y]]);
    //echo("Arc points ", points);
    polygon(points=points);
}

/*
 * Cylinder modules from https://en.wikibooks.org/wiki/OpenSCAD_User_Manual/undersized_circular_objects
 */
module cylinder_outer(h,r,fn){
   fudge = 1/cos(180/fn);
   cylinder(h=h,r=r*fudge,$fn=fn);}
module cylinder_mid(h,r,fn){
   fudge = (1+1/cos(180/fn))/2;
   cylinder(h=h,r=r*fudge,$fn=fn);}

