#!/bin/bash

OPTS="-Dparameter_lego_stud_lines=0 -Dparameter_bend_angle=0 -Dparameter_crossing=false -Dparameter_female_end=true -Dparameter_iron_track=false --render"

set -x

openscad $OPTS -Dparameter_length=208 -Dparameter_lego_stud_lines=1 -o straight-legox1-208mm.stl rails.scad
openscad $OPTS -Dparameter_length=136 -Dparameter_lego_stud_lines=1 -o straight-legox1-136mm.stl rails.scad
openscad $OPTS -Dparameter_length=112 -Dparameter_lego_stud_lines=1 -o straight-legox1-112mm.stl rails.scad
openscad $OPTS -Dparameter_length=208 -Dparameter_lego_stud_lines=2 -o straight-legox2-208mm.stl rails.scad
openscad $OPTS -Dparameter_length=136 -Dparameter_lego_stud_lines=2 -o straight-legox2-136mm.stl rails.scad
openscad $OPTS -Dparameter_length=112 -Dparameter_lego_stud_lines=2 -o straight-legox2-112mm.stl rails.scad

openscad $OPTS -Dparameter_length=215 -o straight-215mm.stl rails.scad
openscad $OPTS -Dparameter_length=143 -o straight-143mm.stl rails.scad
openscad $OPTS -Dparameter_length=117 -o straight-117mm.stl rails.scad

openscad $OPTS -Dparameter_length=215 -Dparameter_symetrical=true -o straight-215mm-sym.stl rails.scad
openscad $OPTS -Dparameter_length=143 -Dparameter_symetrical=true -o straight-143mm-sym.stl rails.scad
openscad $OPTS -Dparameter_length=117 -Dparameter_symetrical=true -o straight-117mm-sym.stl rails.scad

openscad $OPTS -Dparameter_length=80 -Dparameter_bend_angle=0 -Dparameter_female_end=false -Dparameter_crossing=false -o straight-80mm.stl rails.scad
openscad $OPTS -Dparameter_length=80 -Dparameter_bend_angle=0 -Dparameter_female_end=true -Dparameter_crossing=false -o straight-80mm-female.stl rails.scad
openscad $OPTS -Dparameter_length=160 -Dparameter_bend_angle=0 -Dparameter_female_end=false -Dparameter_crossing=false -o straight-160mm.stl rails.scad
openscad $OPTS -Dparameter_length=80 -Dparameter_bend_angle=0 -Dparameter_female_end=false -Dparameter_crossing=true -o straight-80mm-crossing.stl rails.scad

openscad $OPTS -Dparameter_length=84 -Dparameter_bend_angle=45 -o bent-45deg-84mm.stl rails.scad
openscad $OPTS -Dparameter_length=128 -Dparameter_bend_angle=45 -o bent-45deg-128mm.stl rails.scad
openscad $OPTS -Dparameter_length=160 -Dparameter_bend_angle=45 -o bent-45deg-160mm.stl rails.scad
openscad $OPTS -Dparameter_length=84 -Dparameter_bend_angle=45 -Dparameter_symetrical=true -o bend-45deg-84mm-sym.stl rails.scad
openscad $OPTS -Dparameter_length=128 -Dparameter_bend_angle=45 -Dparameter_symetrical=true -o bend-45deg-128mm-sym.stl rails.scad
openscad $OPTS -Dparameter_length=160 -Dparameter_bend_angle=45 -Dparameter_symetrical=true -o bend-45deg-160mm-sym.stl rails.scad

openscad $OPTS -Dparameter_length=80 -Dparameter_iron_track=true -o straight-iron-80mm.stl rails.scad
openscad $OPTS -Dparameter_length=80 -Dparameter_iron_track=true -o straight-iron-80mm.png rails.scad
openscad $OPTS -Dparameter_length=160 -Dparameter_iron_track=true -o straight-iron-160mm.stl rails.scad
openscad $OPTS -Dparameter_length=80 -Dparameter_bend_angle=45 -Dparameter_iron_track=true -o bend-iron-80mm.stl rails.scad


set +x
