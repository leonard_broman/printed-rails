
use <utils.scad>

/* Parts lib. */

//PartsDemo();
//9VModuleDemo();
//9VPowerModule();


module PartsDemo() {
    translate([0,-40,0]) 9VPowerModule();
    Servo9gram();
    translate([0,40,0]) MiniSwitch();
    translate([30,0,0]) ArduinoProMini();
    translate([30,30,0]) MiniBuckConverter1_8A();
    translate([80,0,0]) 9VBattery();
    translate([55,30,0]) SimpleBrick();
}

module 9VModuleDemo() {
    translate([38,21/2,55-3]) rotate([0,0,90]) MiniSwitch();
    translate([33,12+3,10])
        rotate([0,-90,180]) MiniBuckConverter1_8A();
    translate([3,17.5+3,60]) rotate([180,0,0]) 9VBattery();
    9VPowerModule();
}


/*
 * A classical 9 gram servo
 * 
 * TODO Add parameter to render servo horns, maybe different types and at different angles?
 *
*/
Servo9gram(horn_length=15,horn_angle=10);
module Servo9gram(horn_length=undef,horn_angle=0) {
    _length = 23;
    _width = 12;
    _wings_width = 32.5;
    echo("Using 9 gram servo");
    cube([23,_length,_width]);
    translate([4,-4.5,0]) cube([2.5,_wings_width,12]);
    translate([16,_length-0.5,_width/2-5/2]) cube([1,3,5]);
    translate([0,15,_width/2]) rotate([0,-90,0]) cylinder(h=7,r=2,$fn=20); // Spline shaft
    translate([0,15,_width/2]) rotate([0,-90,0]) cylinder(h=4,r=5.5,$fn=20); // Spline shaft
    
    if (horn_length > 0) {
        _thickness = 2;
        translate([-_thickness-7,15,_width/2])
        
        rotate([90,-horn_angle,90]) {
            hull() {
                translate([horn_length,0,0]) cylinder(r=3,h=_thickness);
                cylinder(r=6,h=_thickness);
            }
            translate([0,0,_thickness])cylinder(r=4,h=_thickness);
        }
    }
    
}

/*
 * An arduino pro mini board.
 *
 * Pins are 2.54mm apart
 * Rows are 15.24mm apart
 *
 * TODO make the actual PCB thinner and add the major components instead.
 * TODO Add soldering holes
 *
 *
 *
 */
module ArduinoProMini() {
    l = 34;
    w = 18;
    h = 4;
    _pcb_height = 1.5;
    echo("Using arduino pro mini (l,w,h)",l,w,h);
    difference() {
        union() {
            cube([l,w,_pcb_height]);
            translate([3.4,w/2-6/2,0]) cube([3.5,6,h]);
            translate([14,w/2,2.5/2]) rotate([0,0,45]) cube([8,8,2.5],true);
            translate([22.8,w/2-10/2,0]) cube([3.2,1.5,h]);
            translate([22.8,w/2+10/2-1.5,0]) cube([3.2,1.5,h]);
        }
        for(i=[0:11]) {
            translate([1.5 + i*2.54,w/2-15.24/2,-1]) cylinder(r=0.5,h=h+2);
            translate([1.5 + i*2.54,w/2+15.24/2,-1]) cylinder(r=0.5,h=h+2);
        }
        for(i=[0:5]) {
            translate([l-1.5,w/2-(i-2.5)*2.54,-1]) cylinder(r=0.5,h=h+2);
        }
    }
    
    
}

/*
* Mini DC-DC converter (buck converter) rated 1.8A
* http://www.banggood.com/Small-Mini-360-Adjustable-DC-Power-Supply-Module-Mini-Step-Down-Module-p-917568.html?p=FQ2911554188201404OV
* 
*/

module MiniBuckConverter1_8A() {
    _length = 18;
    _width = 12;
    _height = 4.5;
    _pcb_height = 1.5;
    echo("Using 1.8A buck converter (l,w,h)",_length,_width,_height);
    difference() {
        union() {
            cube([_length,_width,_pcb_height]);
            translate([2.5,4,0]) cube([7.5,7.5,_height]);
            translate([3.5,0,0]) cube([3.5,3.5,_pcb_height+1]);
            translate([3.5+3.4/2,3.4/2,0]) cylinder(r=3.4/2,h=_pcb_height+2,$fn=10); // trim pot
            translate([0.3,_width/2-5/2,0]) cube([2,5,3.5]); // output cap
            translate([_length-2.3,_width/2-5/2,0]) cube([2,5,3.5]); // input cap
        }
        translate([1,1,-1]) cylinder(r=0.5,h=_height+2);
        translate([1,_width-1,-1]) cylinder(r=0.5,h=_height+2);
        translate([_length-1,_width-1,-1]) cylinder(r=0.5,h=_height+2);
        translate([_length-1,1,-1]) cylinder(r=0.5,h=_height+2);
    }
}

/*
* Mini 3/2 position switch
*/
module MiniSwitch() {
    
    echo("Using a mini switch");
    translate([-13/2,-8/2,-10]){
        translate([0,0,0]) cube([13,8,10]);
        translate([13/2,8/2,10]) cylinder(r=3,h=9);
        translate([13/2,8/2,10+7])
        rotate([0,10,0])
        hull() {
            cylinder(r1=1.25,r2=1.5,h=13,$fn=10);
            translate([0,0,13]) sphere(r=1.5,$fn=20);
        }
        for(i=[0:2]) {
            translate([2+i*4,8/2-2/2,-4]) cube([0.5,2,4]);
        }
    }
    
    
    
}

/*
* Attribution: Roman Hegglin http://www.thingiverse.com/thing:155722
*/
module 9VBattery()
{   
    l = 46.4;
    w = 17.5;
    h = 26.5;
    echo("Using 9V battery (l,w,h)",l,w,h);
    r=2;
    translate([r,r,0])
    union(){
        translate(v = [(h-2*r)/2-12.95/2, (w-2*r)/2, l]) {
            cylinder(h = 48.5-46.4, r=3);
        }

        translate(v = [(h-2*r)/2+12.95/2, (w-2*r)/2, l]) {
            for ( i = [0:3])
                { rotate ( i * 60, [0,0,1])
                    cube([7.79,4.5,(48.5-46.4)*2],center = true);
                }
            
        }

        minkowski()
        {
         cube([h-2*r,w-2*r,l/2]);
         cylinder(r=r,h=l/2);
    }
    }
}

module 9VPowerModule() {
    _battery_length=26;
    _width = 21;
    _height = 55;
    _thickness = 3;
    _length = _battery_length + _thickness*2;
    difference(){
        union() {
            cube([_length,_thickness,_height]);
            cube([_thickness,_width,_height]);
            cube([_length,_width,_thickness]);
            translate([_length-_thickness,0,0]) cube([_thickness,_width,_height]);
            translate([_thickness,_width-1,_height/2-20/2]) cylinder(r=1,h=20,$fn=20);
            translate([_length-_thickness,_width-1,_height/2-20/2]) cylinder(r=1,h=20,$fn=20);
            translate([_length,0,0]) cube([5,_thickness,_height]); // power converter edges
            translate([_length,_width-_thickness,0]) cube([5,_thickness,_height]); // power converter edges
        }
        translate([-1,5,_height/2+20/2]) cube([_thickness+2,_width-5,2]);
        translate([-1,5,_height/2-20/2-2]) cube([_thickness+2,_width-5,2]);
        translate([_length-_thickness-1,5,_thickness]) cube([_thickness+2,10,3]); // battery compartment cable passthrough
        translate([_length,-1,_thickness]) cube([6,_thickness+2,3]); // Power external passthrough
        translate([_length/2-5,-1,_thickness]) cube([10,_thickness+2,3]); // Power internal passthrough
        
        // Arthur
        
    }
    translate([_length,0,_height-_thickness])
    difference() {
        union() {
            cube([12,_width,_thickness]);
            rotate([-90,0,0]) Triangle(b=12,h=12,w=_thickness);
            translate([0,_width-_thickness,0]) rotate([-90,0,0]) Triangle(b=12,h=12,w=_thickness);
        }
        translate([5,_width/2,-1]) cylinder(r=3.5,h=_thickness+2);
    }
    
}

/*
 * Simplified lego brick piece. Inspired by J Jansens brick generator http://www.thingiverse.com/thing:178627
 * This brick does only contain the upper studs.
 */

FLU = 1.6; // Fundamental Lego Unit = 1.6 mm

BRICK_WIDTH = 5*FLU; // basic brick width
BRICK_HEIGHT = 6*FLU; // basic brick height
PLATE_HEIGHT = 2*FLU; // basic plate height
WALL_THICKNESS = FLU; // outer wall of the brick
STUD_RADIUS = 1.5*FLU; // studs are the small cylinders on top of the brick with the lego logo ('nopje' in Dutch)
STUD_HEIGHT = FLU; 
PIN_RADIUS = FLU; // a pin is the small cylinder inside bricks that have length = 1 or width = 1
CORRECTION = 0.1; // addition to each size, to make sure all parts connect by moving them a little inside each other

//BrickTop(1,1,bottom_chamfer=true);

module BrickTop(length = 1, width = 1, height = 3,fn=20,bottom_chamfer=false) {
    $fn = fn;
    difference() {
        cube(size = [length*BRICK_WIDTH,width*BRICK_WIDTH,height*PLATE_HEIGHT]);
       if (bottom_chamfer) {
           translate([length*BRICK_WIDTH+0.5,-0.25,-0.25]) rotate([0,-90,0]) Triangle(b=1,h=1,w=length*BRICK_WIDTH+1);
           translate([-0.5,width*BRICK_WIDTH+0.25,-0.25]) rotate([180,-90,0]) Triangle(b=1,h=1,w=length*BRICK_WIDTH+1);
           translate([-0.25,-0.5,-0.25]) rotate([0,-90,-90]) Triangle(b=1,h=1,w=width*BRICK_WIDTH+1);
           translate([length*BRICK_WIDTH+0.25,width*BRICK_WIDTH+0.5,-0.25]) rotate([0,-90,90]) Triangle(b=1,h=1,w=width*BRICK_WIDTH+1);
       }

    }
    translate([STUD_RADIUS+WALL_THICKNESS,STUD_RADIUS+WALL_THICKNESS,height*PLATE_HEIGHT])
        for (y = [0:width-1]){
            for (x = [0:length-1]){
                translate ([x*BRICK_WIDTH,y*BRICK_WIDTH,-CORRECTION])
                cylinder_outer(h=STUD_HEIGHT+CORRECTION, r=STUD_RADIUS,fn=fn);
            }
        }
}


module Triangle(b=10,h=10,w=4) {
        rotate(a=[0,0,0])
            linear_extrude(height = w)
                polygon(points=[[0,0],[h,0],[0,b]], paths=[[0,1,2]]);
}

