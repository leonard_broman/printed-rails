/*
* Simple tunnel for crossings
*/

rail_width=42; // Actual 40
rail_height=12; // Normal rail height
thickness=5;
length=120;
max_train_height = 55;
height=max_train_height+rail_height;
height_overlap=10;

$fn=30;

difference() {
    union() {
        hull() {
            rotate([0,0,90]) translate([-length/2,-(rail_width+thickness*2)/2,0]) cube([length,rail_width+thickness*2,height]);
            translate([0,length/2,height-height_overlap]) rotate([90,0,0]) cylinder(r=rail_width/2+thickness,h=length);
        }

        hull() {
            translate([-length/2,-(rail_width+thickness*2)/2,0]) cube([length,rail_width+thickness*2,height]);
            translate([-length/2,0,height-height_overlap]) rotate([90,0,90]) cylinder(r=rail_width/2+thickness,h=length);
        }
    }

    
    hull() {
        rotate([0,0,90]) translate([-length/2-1,-(rail_width)/2,-thickness/2-1]) cube([length+2,rail_width,height+1]);
        translate([0,length/2+1,height-height_overlap]) rotate([90,0,0]) cylinder(r=rail_width/2,h=length+2);
    }
    
    hull() {
        translate([-length/2-1,-(rail_width)/2,-1-thickness/2]) cube([length+2,rail_width,height+1]);
        translate([-length/2-1,0,height-height_overlap]) rotate([90,0,90]) cylinder(r=rail_width/2,h=length+2);
    }

}


