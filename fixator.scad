
fixation_center_offset = 11;
fixation_neck_width = 7;
fixation_neck_depth = 3.5;

fixator_center_offset = 9.5;
fixator_neck_width = 6;

render() {
    Fixator(half=false,height=8);
    translate([0,20,0]) FemaleFixationHole(extra_neck_length=2);
    translate([0,-20,0]) HalfFixator();
}

module Fixator(half=false,height=8) {
    // TODO Use mirror to generate the full instead of cutting in half
    if (half) {
        HalfFixator(height=height);
    } else {
        HalfFixator(height=height);
        mirror([1,0,0]) HalfFixator(height=height);
    }
}

module HalfFixator(height=8) {
    radius = 6;
    translate([0,0,0])
    difference() {
        union() {
//            translate([-fixator_center_offset,0,0]) cylinder(r=radius,h=height);
            translate([fixator_center_offset,0,0]) cylinder(r=radius,h=height);
            translate([0,-fixator_neck_width/2,0]) 
                cube([fixator_center_offset,fixator_neck_width,height]);
        }
        translate([0,0,-0.5])
        for (i = [-1,1]) {
            hull() {
                translate([radius-2.5,i*(fixator_neck_width/2+0.5),0]) cylinder(r=0.7,h=height+1);
                translate([fixator_center_offset+1.5,i*(fixator_neck_width/2+0.5),0]) cylinder(r=0.7,h=height+1);
            }
            
        }
    }
}

module FixatorPinhole(fixator_height=8) {
    radius = 1.5;
    hull() {
            translate([-fixator_center_offset-10,0,fixator_height-radius-1])
                rotate([0,90,0]) cylinder(r=radius,h=fixator_center_offset*2+20,$fn=20);
            translate([-fixator_center_offset-10,0,radius+1])
                rotate([0,90,0]) cylinder(r=radius,h=fixator_center_offset*2+20,$fn=20);
        }
}


module FemaleFixationHole(extra_neck_length=0,height=12) {
    //rotate_extrude() polygon(points=[[0,0],[0,7],[]]);
    translate([fixation_center_offset,0,height-2]) cylinder(h=3,r1=6,r2=9);
    translate([fixation_center_offset,0,-1]) cylinder(h=3,r1=9,r2=6);
    linear_extrude(height=height)
    polygon(points=[[0-extra_neck_length,-fixation_neck_width/2]
                    ,[fixation_neck_depth,-fixation_neck_width/2]
                    ,[fixation_center_offset-2,-fixation_neck_width/2-3.0]
                    ,[fixation_center_offset-2,fixation_neck_width/2+3.0]
                    ,[fixation_neck_depth,fixation_neck_width/2]
                    ,[0-extra_neck_length,fixation_neck_width/2]]);
    //translate([0,-fixation_neck_width/2,0]) cube([fixation_center_offset,fixation_neck_width,height]);
    translate([fixation_center_offset,0,0]) cylinder(r=7,h=height);
    chamfer_profile = [[0,-fixation_neck_width/2]
                        ,[1,-fixation_neck_width/2-1]
                        ,[2,-fixation_neck_width/2-1]
                        ,[2,fixation_neck_width/2+1]
                        ,[1,fixation_neck_width/2+1]
                        ,[0,fixation_neck_width/2]];
    translate([fixation_center_offset,0,height-1]) rotate([0,-90,0]) linear_extrude(height=fixation_center_offset+extra_neck_length) polygon(points=chamfer_profile);
    translate([-extra_neck_length,0,1]) rotate([0,90,0]) linear_extrude(height=fixation_center_offset+extra_neck_length) polygon(points=chamfer_profile);
}