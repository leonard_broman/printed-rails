
base_length = 70;
base_height = 12;
base_width = 30;
edge_offset = 15;
frame_spacing = 0.5;

rotate([0,-90,0]) GateHolder();

module Demo() {

    render() {
        GateHolder();
        translate([3+frame_spacing,edge_offset-4,base_height+frame_spacing]) rotate([90,0,90]) Servo();
        translate([30,4,6]) ArduinoProMini();

        translate([-100,0,0]) {
            PowerBox();
            //translate([80,4,6]) rotate([0,0,90]) VoltageConverter();
            translate([71,6,4]) rotate([90,0,90]) VoltageConverter();
            translate([5,3+3,5]) rotate([90,0,90]) 9V();
        }
    }

}

module GateHolder() {
    frame_width = 3;
    servo_width = 12;
    servo_length = 23;
    difference() {
        union() {
            cube([base_length,base_width,base_height]);
            translate([0,edge_offset,base_height]) cube([servo_width+frame_width*2+frame_spacing*2,5,servo_length+5+3]); // Servo tower
            //translate([0,edge_offset+2,base_height+30]) cube([25,3,10]);  // Diode holder
            translate([0,edge_offset+2,base_height+30])
            hull() {
                $fn=20;
                cube([5,3,5]);
                translate([2.5,0,5]) rotate([-90,0,0]) cylinder(h=3,r=2.5);
                translate([25,0,5]) rotate([-90,0,0]) cylinder(h=3,r=2.5);
                translate([25,0,2.5]) rotate([-90,0,0]) cylinder(h=3,r=2.5);
            }
            
        }
        translate([frame_width,edge_offset-4,base_height-5]) cube([12+frame_spacing*2,8,34]); // Servo inset
        translate([frame_width,edge_offset-4,base_height+0.1]) cube([12+frame_spacing*2,30,23+frame_spacing*2]); // Servo slot
        
        // Diode holder slots
        translate([5,edge_offset+2,base_height+30+7.5/2]) rotate([-90,0,0]) cylinder(h=4,r=1.5,$fn=20);
        translate([22.5,edge_offset+2,base_height+30+7.5/2]) rotate([-90,0,0]) cylinder(h=4,r=1.5,$fn=20);
        
        
        // Slot for the pro mini
        translate([29,3,base_height-8]) cube([37,20,8]);
        // Cover inset for the slot
        translate([28,2,base_height-2]) cube([39,22,3]);
        
        // Slot for the cables to/from pro mini
        translate([-1,5,5]) SignalSlot();
        translate([-1,5+16,5]) SignalSlot();
    }

        
}

module PowerBox() {
    
    _height = 18;
    _width = 35;
    _length = 80;
    connector_space = 5;
    voltage_converter_dims = [17,21,5];
    battery_dims = [46.4, 26.5, 17.5];
    difference() {
        cube([_length,_width,_height]);
        translate([3,3,1]) cube([battery_dims[0]+4+connector_space,_width-3*2,_height+2]);
        translate([_length-10,5,3]) cube([7,23,_height]); // Slot for voltage converter
        translate([_length-29,5,5]) SignalSlot(length=30);
        translate([_length-29,5+16,5]) SignalSlot(length=30);
    }
    
}

module SignalSlot(length=undef) {
    _length = length == undef ? base_length+2 : length;
        rotate([90,180,90])
            linear_extrude(height=_length) hull() {
                _radius = 2;
                circle(r=_radius,$fn=20);
                translate([-_radius,-1]) square([_radius*2,8]);
            }
}

module Servo() {
    _length = 23;
    _width = 12;
    _wings_width = 32.5;
    cube([23,_length,_width]);
    translate([4,-4.5,0]) cube([2.5,_wings_width,12]);
    translate([16,_length-0.5,_width/2-5/2]) cube([1,3,5]);
    translate([0,15,_width/2]) rotate([0,-90,0]) cylinder(h=7,r=2,$fn=20); // Spline shaft
    translate([0,15,_width/2]) rotate([0,-90,0]) cylinder(h=4,r=5.5,$fn=20); // Spline shaft
}

module ArduinoProMini() {
    cube([35,18,3]);
}

module VoltageConverter() {
    $fn=20;
    _width = 17;
    _length = 21;
    difference() {
        cube([_length,_width,5]);
        translate([1,1.5,0]) cylinder(r=0.5,h=7);
        translate([1,1.5+2.25,0]) cylinder(r=0.5,h=7);
        translate([1,_width-1.5,0]) cylinder(r=0.5,h=7);
        translate([1,_width-1.5-2.25,0]) cylinder(r=0.5,h=7);
        
        translate([_length-1,1.5,0]) cylinder(r=0.5,h=7);
        translate([_length-1,1.5+2.25,0]) cylinder(r=0.5,h=7);
        translate([_length-1,_width-1.5,0]) cylinder(r=0.5,h=7);
        translate([_length-1,_width-1.5-2.25,0]) cylinder(r=0.5,h=7);
    }
    
}

/*
* Attribution: Roman Hegglin http://www.thingiverse.com/thing:155722
*/
module 9V()
{
    r=2;
    union(){
        translate(v = [(26.5-2*r)/2-12.95/2, (17.5-2*r)/2, 46.4]) {
            cylinder(h = 48.5-46.4, r=3);
        }

        translate(v = [(26.5-2*r)/2+12.95/2, (17.5-2*r)/2, 46.4]) {
            for ( i = [0:3])
                { rotate ( i * 60, [0,0,1])
                    cube([7.79,4.5,(48.5-46.4)*2],center = true);
                }
            
        }

        minkowski()
        {
         cube([26.5-2*r,17.5-2*r,46.4/2]);
         cylinder(r=r,h=46.4/2);
    }
    }
}