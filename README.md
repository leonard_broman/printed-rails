# README #

This is a OpenSCAD code base for creating 3d-printable track pieces for brio railway system.


## generate-stl.sh ##

The simple bash shell is made for simply regenerating exported track piece STL:s. Those are the ones I normally publish to thingiverse.

# Compatibility #

## Original brio trains ##

The classic brio trains, from the eighties and before, are much smaller than the newer ones.
Due to this many old track pieces will not allow newer trains to pass (like the wood dispenser).
The parts measures 30mm in height (from the track) and 20mm in width, excluding wheels.

## Newer brio trains ##

Newer trains have gotten a bit bigger. Especially the more recent and advanced parts,
like the electrified and remote controlled 33213 engine set.

Those cars often measures 35mm in width and upto 55mm in height (from the track).

## Noname brio-compatible trains ##

## Chuggington wooden trains ##

The chuggington wooden series trains work well with the Brio tracks. However, the
train chassis is larger than most other trains. The widest part of the train measures
35mm and the highest point, when the train is placed on the track, measures 60mm.

They use magnets for train-carriage attachment. They're located slightly higher than on
brio carriges but they're still compatible.



[Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
