/*
 * Turntable
 */

use <../track.scad>;
use <../fixator.scad>;
use <../rails.scad>;
use <../utils.scad>;

// Brio rails constants 
total_width=40;
total_height=12;
track_gap = 12;

// Turntable constants
base_thickness = 4;

//render() Assembly();
module Assembly() {
    outer_radius=90;
    inner_radius=65;
    angle_spacing = 25;
    turntable_angles = [angle_spacing*0.5,-angle_spacing*0.5,angle_spacing*1.5,-angle_spacing*1.5,180];
    attachment_angles = [angle_spacing*0.5,-angle_spacing*0.5,angle_spacing*1.5,-angle_spacing*1.5];
    RoundHouseAttachment(outer_radius=outer_radius,inner_radius=inner_radius,track_angles=attachment_angles); TurnTableBase(outer_radius=outer_radius,inner_radius=inner_radius,track_angles = turntable_angles);
    
    translate([0,0,1]) 608Bearing();
    rotate([0,0,angle_spacing*0.5])
            translate([0,0,base_thickness+1]) TurnTableRails(outer_radius=inner_radius-1);
    translate([0,0,0.5]) CenterPin();
}

render() CenterPin();
module CenterPin() {
    difference() {
        ChamferedCylinder(h=total_height+1,r=4.2,chamfer=0.5);
        translate([-1/2,-2,-1]) cube([1,10,total_height+2]);
    }
}

module ChamferedCylinder(h=1,r=1,chamfer=0.1) {
    union() {
        cylinder(r1=r-chamfer,r2=r,h=chamfer);
        translate([0,0,chamfer]) cylinder(r=r,h=h-chamfer*2);
        translate([0,0,h-chamfer]) cylinder(r1=r,r2=r-chamfer,h=chamfer);
    }
}

//render() TurnTableRails();
module TurnTableRails(outer_radius=39) {
    _length = 160;
    _outer_radius = outer_radius;
    _sleeper_length = _outer_radius*2+10;
    translate([0,0,-base_thickness])
    difference() {
        union() {
            translate([-_length/2,0,0]) StraightTracks(profile=true,length=_length);
            translate([-_sleeper_length/2,0,0]) StraightSleepers(length=_sleeper_length,symetrical=true);
            cylinder(r=14,h=10);
            cylinder(r=6,h=total_height+1);
        }
        difference() {
            cylinder(r=100,h=12);
            cylinder(r=_outer_radius,h=12);
        }
        translate([_length/2,0,0]) rotate([0,-90,0]) linear_extrude(height=_length) FullTrackProfile(track_cutouts=true);
        cylinder(r=12,h=8);
        translate([-_length/2,-_length/2,0]) cube([_length,_length,base_thickness]);
        cylinder(r=4,h=total_height+1);
    }
    
}
/*
render() {
    outer_radius=70;
    RoundHouseAttachment(outer_radius=outer_radius,track_angles=[35*0.5,-35*0.5,35*1.5,-35*1.5]); TurnTableBase(outer_radius=outer_radius,track_angles = [35*0.5,-35*0.5,35*1.5,-35*1.5,180]);
} */
module RoundHouseAttachment(outer_radius=64,track_angles = [0,45,-45]) {
    flattening_distance = sqrt(pow(outer_radius,2)-pow(total_width/2,2));
    track_length = 25;
    difference() {
        union() {
            rotate([0,0,90]) linear_extrude(height=total_height)
            difference() {
                PieceOfPie(radius=outer_radius+10,start=min(track_angles),end=max(track_angles));
                PieceOfPie(radius=outer_radius+1,start=min(track_angles),end=max(track_angles));
            }
           for (a = track_angles) {
            rotate([0,0,a+180]) translate([flattening_distance+1,-total_width/2,0]) cube([track_length,total_width,total_height]);
    }

        }
        for (a = track_angles) {
                rotate([a,-90,0]) linear_extrude(height=outer_radius+track_length+10) FullTrackProfile(track_cutouts=true);
                rotate([0,0,a+180]) translate([flattening_distance,0,0]) rotate([0,0,0]) FemaleFixationHole(extra_neck_length=1);
        }
    }
}

//render() TurnTableBase(); 
module TurnTableBase(outer_radius = 64,inner_radius=40,track_angles = [0,45,-45,180]) {
    flattening_distance = sqrt(pow(outer_radius,2)-pow(total_width/2,2));
    //track_angles = [0,45,-45,180];
    //track_angles = [0,35,70,-35,-70,180];
    //track_angles = [35*0.5,-35*0.5,35*1.5,-35*1.5,180];
    difference() {
        cylinder(r=outer_radius,h=12);
        translate([0,0,base_thickness]) cylinder(r=inner_radius,h=12); // Large inner hole
        translate([0,0,0.5]) cylinder(r=11,h=12); // Bearing hole
        translate([0,0,-0.5]) cylinder(r=7,h=12); // Bearing hole

        
        for (a = track_angles) {
            rotate([a,-90,0]) linear_extrude(height=outer_radius+10) FullTrackProfile(track_cutouts=true);
            rotate([0,0,a+180]) translate([flattening_distance,-50/2,-1]) cube([20,50,total_height+10]);
            rotate([0,0,a+180]) translate([flattening_distance,0,0]) rotate([0,0,180]) FemaleFixationHole(extra_neck_length=1);
        }
        /*
        rotate([0,-90,0]) linear_extrude(height=80) FullTrackProfile(track_cutouts=true);
        rotate([0,0,0]) translate([-outer_radius-flattening_compensation-20,-50/2,-1]) cube([20,50,total_height+10]);
        
        rotate([38,-90,0]) linear_extrude(height=80) FullTrackProfile(track_cutouts=true);
        rotate([0,0,38]) translate([-outer_radius-flattening_compensation-20,-50/2,-1]) cube([20,50,total_height+10]);
        rotate([-38,-90,0]) linear_extrude(height=80) FullTrackProfile(track_cutouts=true);
        rotate([0,0,-38]) translate([-outer_radius-flattening_compensation-20,-50/2,-1]) cube([20,50,total_height+10]);
        rotate([180,-90,0]) linear_extrude(height=80) FullTrackProfile(track_cutouts=true);
        rotate([0,0,180]) translate([-outer_radius-flattening_compensation-20,-50/2,-1]) cube([20,50,total_height+10]); */
        
    }
    
}


//render() 608Bearing();
module 608Bearing() {
     difference() {
        cylinder(r=11,h=7);
        translate([0,0,-1]) cylinder(r=4,h=9);
     }
 }