
use <../track.scad>;
use <../utils.scad>;
use <../rails.scad>;
use <../parts.scad>;

/*
 * This is supposed to be a clone of an old wood/log dispensing station.
 * The old part is problematic as newer trains cannot pass under it (as my son pointed out).
 * This station will be slighlty higher to allow trains to pass properly.
 
 * Original dimensions
 * height: 100mm
 * inner spacing: 30mm
 * height track to slider bottom: 40mm
 * track length: 145mm
 
 * log length: 35mm
 * log diameter: 8mm
 *
 
 *
 * TODO Add cable transfers from the battery to the voltage regulator
 * TODO Elaborate on placement of voltage regulater
 * TODO Add a nicer rear end position stop for the feeder. This can make the feeder shorter and not stick out when retracted.
 * TODO Add push button part and add to demo layout
 * TODO Add something to make the attachment to the track better.
 * TODO Add an attachment thingy for the battery
 *
 */

/* Constants from elsewhere */
track_height = 12;
max_train_height = 60;

/* Constants for this model */
platform_height = track_height + max_train_height;
thickness = 12;
dispenser_top_height = 48;
depth = 40;


LogsPrint();
//DispenserPrint();
//Demo();
//DispenserBase();
//DispenserTop();
//Feeder();
//FeedPushBar();


module LogsPrint() {
    Log();
    /* translate([14,0,0]) Log();
    translate([14,14,0]) Log();
    translate([0,14,0]) Log(); */
}

module DispenserPrint() {
    translate([0,40,platform_height]) rotate([180,0,0]) DispenserBase();
    translate([0,100,dispenser_top_height]) rotate([180,0,0]) DispenserTop();
    translate([68,0,7]) rotate([180,0,90]) Feeder();
}

module Demo() {
    translate([0,-20,0]) SimpleStraightRails(length = 145);
    render() {
        translate([50,0,0]) {
            DispenserBase();
            translate([0,-5,platform_height]) DispenserTop();
             translate([0,5+22.5,0]) // Rear position of feeder
            translate([6.5,depth+thickness-2,platform_height+1]) rotate([0,0,-90]) Feeder();
            
            translate([9,10,platform_height + 12]) rotate([0,90,0]) Log();
            translate([9,10,platform_height + 20]) rotate([0,90,0]) Log();
            translate([62,24,platform_height - 40]) rotate([-90,180,0]) Servo9gram(horn_length=26,horn_angle=20);
            //translate([-2,5,0]) rotate([0,0,90]) 9VBattery();
            translate([0,0,0]) rotate([0,0,90]) 9VModuleDemo();
            
            translate([18+thickness + 6,6,15]) rotate([0,-90,90]) ArduinoProMini();
           
            translate([55+10,38,platform_height+4-6]) rotate([10,0,15]) rotate([90,0,90]) FeedPushBar();
        }
    }
    
}

module Feeder() {
    corner_radius = 1;
    _height = 7;
    _width = 39;
    _length = 40 + _height + 35;
    _slot_width = 8;
    _slot_x_pos = 40 + _height + 20 - _slot_width/2;
    $fn=20;
    difference() {
        union() {
            translate([corner_radius,corner_radius,0])
            hull() {
                translate([0,0,0]) cylinder(r=corner_radius,h=_height);
                translate([_length,0,0]) cylinder(r=corner_radius,h=_height);
                translate([_length,_width,0]) cylinder(r=corner_radius,h=_height);
                translate([0,_width,0]) cylinder(r=corner_radius,h=_height);
            }
            translate([corner_radius,corner_radius,0])
            hull() {
                translate([0,-_height,0]) cylinder(r=corner_radius,h=_height);
                translate([_height,-_height,0]) cylinder(r=corner_radius,h=_height);
                translate([_height,_width+_height,0]) cylinder(r=corner_radius,h=_height);
                translate([0,_width+_height,0]) cylinder(r=corner_radius,h=_height);
            }
            //translate([_length-3,_width/2-5,-4]) cube([3,10,4]);  //cylinder(r=4,h=5);
            translate([_length-2,1.5,-4]) cube([3,4,4]);
            translate([_length-2,_width-4+1-0.5,-4]) cube([3,4,4]);
        }
        translate([corner_radius,corner_radius,0])
        hull() {
            translate([_slot_x_pos,0+2,0]) cylinder(r=corner_radius,h=_height);
            translate([_slot_x_pos+_slot_width,0+2,0]) cylinder(r=corner_radius,h=_height);
            translate([_slot_x_pos+_slot_width,_width-2,0]) cylinder(r=corner_radius,h=_height);
            translate([_slot_x_pos,_width-2,0]) cylinder(r=corner_radius,h=_height);
        }
    }
}


module FeedPushBar() {
    _distance = 36;
    _thickness = 2;
    $fn=20;
    difference() {
        hull() {
            cylinder(r=3.5,h=_thickness);
            translate([_distance,0,0]) cylinder(r=3.5,h=_thickness);
        }
        translate([0,0,-1]) cylinder(r=1.5,h=_thickness+2);
        translate([_distance,0,-1]) cylinder(r=1.5,h=_thickness+2);

    }
    
}

module DispenserBase() {
    
    
    _spacing = 30;
    _thickness = thickness;
    // _height = 100;
    _depth = depth;
    _width = _spacing+_thickness*2;
    _extension_depth = 5;
    _top_platform_thickness = _thickness - 6;
    _extension_height = platform_height - _top_platform_thickness - 5;
    x_offset = 0;

    translate([_thickness+5,_depth-5,platform_height-_top_platform_thickness]) rotate([0,180,180]) linear_extrude(height=1) text(text="Arthur",size=6);
    
    sides_profile = [[0,0],[_extension_height,0],[_extension_height+_extension_depth,-_extension_depth],[platform_height,-5],[platform_height,_depth],[0,_depth]];
    difference() {
        union() {
            translate([_thickness,0,0]) rotate([0,-90,0]) linear_extrude(height=_thickness) polygon(points=sides_profile);        
            translate([_spacing+_thickness+_thickness,0,0]) rotate([0,-90,0]) linear_extrude(height=_thickness) polygon(points=sides_profile);
            translate([0,_depth/2-_thickness/2-6,0]) cube([_width,_thickness-2,platform_height]); // Center support
            translate([0,-5,platform_height-_top_platform_thickness]) cube([_width,_depth+5,_top_platform_thickness]); // Platform bottom
            
        }
        /* Holes for simple attachment of the top part */
        translate([(thickness-5)/2,(thickness-5)/2-_extension_depth,platform_height-_top_platform_thickness]) cylinder_outer(r=1.5,h=_top_platform_thickness,fn=10);
        translate([(thickness-5)/2,_depth-(thickness-5)/2,platform_height-_top_platform_thickness]) cylinder_outer(r=1.5,h=_top_platform_thickness+1,fn=10);
        translate([_width-(thickness-5)/2,(thickness-5)/2-_extension_depth,platform_height-_top_platform_thickness]) cylinder_outer(r=1.5,h=_top_platform_thickness+1,fn=10);
        translate([_width-(thickness-5)/2,_depth-(thickness-5)/2,platform_height-_top_platform_thickness]) cylinder_outer(r=1.5,h=_top_platform_thickness+1,fn=10);
        
        // Servo slot
        translate([_width-_thickness-1,_depth/2+thickness/2-2.5,platform_height-_thickness-10-19]) cube([20,13,25]);
        
        /* Cable slots for the arduino */
        _connector_slot_width = 3;
        translate([_width/2-15.24/2-_connector_slot_width/2,7,15]) cube([_connector_slot_width,_thickness,35]);
        translate([_width/2+15.24/2-_connector_slot_width/2,7,15]) cube([_connector_slot_width,_thickness,35]);
        
        /* Cable channels to dispenser top */
        translate([_width/2-(_width-8)/2,_depth/2,platform_height-_thickness]) cube([_width-8,5,5]);
        translate([_width-5-4,_depth/2,platform_height-_thickness])  cube([5,5,_thickness+2]);
        translate([4,_depth/2,platform_height-_thickness])  cube([5,5,_thickness+2]);
        
        /* Cable passages to the power module */
        translate([0,3,3]) cube([_thickness,6,3]);
        translate([0,16,3]) cube([_thickness,6,3]);
        translate([0,_depth-3-6,3]) cube([_thickness,6,3]);
        
        /* Insets for the end stoppers of the dispenser slider */
        translate([_width/2-39/2,-_extension_depth,platform_height-4]) cube([5,5,4+1]);
        translate([_width/2+39/2-5,-_extension_depth,platform_height-4]) cube([5,5,4+1]);
        
        /*
        translate([thickness/2,thickness/2,platform_height-9]) cylinder(r=2,h=10,$fn=20);
        translate([thickness/2,_depth-thickness/2,platform_height-9]) cylinder(r=2,h=10,$fn=20);
        translate([_width-thickness/2,thickness/2,platform_height-9]) cylinder(r=2,h=10,$fn=20);
        translate([_width-thickness/2,_depth-thickness/2,platform_height-9]) cylinder(r=2,h=10,$fn=20);     
        */
    //    translate([_width/2-(_spacing+10)/2,-1-5,platform_height]) cube([_spacing+10,_depth+2+5,9]); // Slider slot
      //  translate([_width/2-(_spacing+10)/2,5,platform_height]) cube([_spacing+10,9,_height]); // Timber feed slots
    } 
}

module DispenserTop() {
    _extension_height = 35;
    _spacing = 30;
    _thickness = thickness;
    _height = dispenser_top_height;
    _depth = depth+5;
    _width = _spacing+_thickness*2;
    _timber_slow_width = 37;
    
    x_offset = 0;

    sides_profile = [[0,0],[_height,0],[_height,25],[_height-_depth,_depth],[0,_depth]];
    support_profile = [[_height,0],[_height-8,0],[_height,8]];
    
    translate([_width,12.5,_height-2])
    difference() {
        union() {
            cylinder(r=12.5,h=2);
            translate([0,-6,-10])
            difference() {
                cube([10,2,10]);
                translate([10,3,0]) rotate([90,0,0]) cylinder(r=10,h=4);
            }
            translate([0,4,-10])
            difference() {
                cube([10,2,10]);
                translate([10,3,0]) rotate([90,0,0]) cylinder(r=10,h=4);
            }
                
        }
        translate([-20,-20,-1]) cube([20,40,5]);
        translate([12.5/2,0,-1]) cylinder(r=3.5,h=4);
    }
    

    difference() {
        union() {
            translate([_width,0,0]) rotate([0,-90,0]) linear_extrude(height=_width) polygon(points=support_profile);
            translate([_thickness,0,0]) rotate([0,-90,0]) linear_extrude(height=_thickness) polygon(points=sides_profile);        
            translate([_spacing+_thickness+_thickness,0,0]) rotate([0,-90,0]) linear_extrude(height=_thickness) polygon(points=sides_profile);
            translate([(thickness-5)/2,(thickness-5)/2,-2]) cylinder(r=1.5,h=2,$fn=20);
            translate([(thickness-5)/2,_depth-(thickness-5)/2,-2]) cylinder(r=1.5,h=2,$fn=20);
            translate([_width-(thickness-5)/2,(thickness-5)/2,-2]) cylinder(r=1.5,h=2,$fn=20);
            translate([_width-(thickness-5)/2,_depth-(thickness-5)/2,-2]) cylinder(r=1.5,h=2,$fn=20);
        }
        translate([_width/2-(_spacing+12)/2,-1,0]) cube([_spacing+12,_depth+2+5,9]); // Slider slot
        translate([_width/2-_timber_slow_width/2,10,0]) cube([_timber_slow_width,9,_height]); // Timber feed slots
        
        /* Cable transfer to push button */
        translate([_width-5-4,_depth/2,0]) cube([5,5,_height-10]);
        //translate([_width-5,_depth/2,_height-15]) cube([10,5,5]);
        translate([_width-5,_depth/2+2.5,_height-15+2.5]) rotate([0,90,0]) cylinder(r=2.5,h=10,$fn=20);
        
   } 
}


module Log() {
    _radius = 4;
    _length = 35;
    //cylinder(r=_radius,h=_length);
    rotate_extrude($fn=20) polygon(points=[[0,0],[_radius-1,0],[_radius,1],[_radius,_length-1],[_radius-1,_length],[0,_length]]);
}


